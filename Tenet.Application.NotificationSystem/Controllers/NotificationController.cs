﻿using Microsoft.AspNetCore.Mvc;
using Tenet.Application.NotificationSystem.Api.Dtos;
using Tenet.Application.NotificationSystem.Core.Entities;
using Tenet.Application.NotificationSystem.Core.Interfaces;

namespace Tenet.Application.NotificationSystem.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class NotificationController : ControllerBase
    {
        private readonly IEventQueue _eventQueue;

        public NotificationController(IEventQueue eventQueue)
        {
            _eventQueue = eventQueue;
        }

        [HttpPost]
        public IActionResult SendNotification(NotificationRequestDto request)
        {
            if (request == null || request.Channels == null || request.Channels.Count == 0)
            {
                return BadRequest("Invalid request body.");
            }

            var @event = new Event(request.EventType, request.Channels);
            _eventQueue.Enqueue(@event);

            return Ok("Notification queued for processing.");
        }
    }
}
