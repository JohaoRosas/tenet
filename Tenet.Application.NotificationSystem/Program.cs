using Tenet.Application.NotificationSystem.Core.Converters;
using Tenet.Application.NotificationSystem.Core.Interfaces;
using Tenet.Application.NotificationSystem.Core.Services;
using Tenet.Application.NotificationSystem.Infrastructure.Data;
using Tenet.Application.NotificationSystem.Infrastructure.Email;
using Tenet.Application.NotificationSystem.Infrastructure.Providers.Email;
using Tenet.Application.NotificationSystem.Infrastructure.Providers.PushNotifications;
using Tenet.Application.NotificationSystem.Infrastructure.Providers.Sms;
using Tenet.Application.NotificationSystem.Infrastructure.PushNotifications;
using Tenet.Application.NotificationSystem.Infrastructure.SMS;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers().AddJsonOptions(options =>
{
    options.JsonSerializerOptions.Converters.Add(new NotificationDataConverter());
});

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
// Add services to the container.
builder.Services.AddSingleton<IEventQueue, InMemoryEventQueue>();
builder.Services.AddSingleton<ITemplateEngine, TemplateEngine>();
builder.Services.AddSingleton<IEmailService, SmtpEmailService>();
builder.Services.AddSingleton<ISmsService, SmsApiService>();
builder.Services.AddSingleton<IPushNotificationService, PushNotificationApiService>();

//builder.Services.AddSingleton<ISmsServiceProvider, TwilioSmsServiceProvider>();
builder.Services.AddSingleton<ISmsServiceProvider, NexmoSmsServiceProvider>();
builder.Services.AddTransient<ISmsService, SmsApiService>();

builder.Services.AddSingleton<IPushNotificationServiceProvider, AndroidPushNotificationServiceProvider>();
builder.Services.AddTransient<IPushNotificationService, PushNotificationApiService>();

builder.Services.AddSingleton<IEmailServiceProvider, SendGridEmailServiceProvider>();
builder.Services.AddTransient<IEmailService, SmtpEmailService>();

builder.Services.AddTransient<INotificationChannel, EmailNotificationChannel>();
builder.Services.AddTransient<INotificationChannel, SmsNotificationChannel>();
builder.Services.AddTransient<INotificationChannel, PushNotificationChannel>();

builder.Services.AddHostedService<NotificationProcessor>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
