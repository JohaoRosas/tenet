﻿using Tenet.Application.NotificationSystem.Core.Interfaces;
using Tenet.Application.NotificationSystem.Core.Models;

namespace Tenet.Application.NotificationSystem.Api.Dtos
{
    public class NotificationRequestDto
    {
        public string EventType { get; set; }
        public List<NotificationChannel> Channels { get; set; }
    }
}
