﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tenet.Application.NotificationSystem.Shared.Utils
{
    public static class NotificationUtils
    {
        public static string FormatMessage(string template, Dictionary<string, string> data)
        {
            foreach (var key in data.Keys)
            {
                template = template.Replace($"{{{{{key}}}}}", data[key]);
            }
            return template;
        }
    }
}
