﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tenet.Application.NotificationSystem.Core.Models
{
    public class NotificationChannel
    {
        public string Type { get; set; }
        public NotificationData Data { get; set; }
    }
}
