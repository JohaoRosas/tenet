﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tenet.Application.NotificationSystem.Core.Models
{
    public class SmsNotificationData : NotificationData
    {
        public string PhoneNumber { get; set; }
        public string Message { get; set; }
    }
}
