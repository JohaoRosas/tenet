﻿                using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tenet.Application.NotificationSystem.Core.Models;

namespace Tenet.Application.NotificationSystem.Core.Entities
{
    public class Event
    {
        public string Type { get; set; }
        public List<NotificationChannel> Channels { get; set; }

        public Event(string type, List<NotificationChannel> channels)
        {
            Type = type;
            Channels = channels ?? new List<NotificationChannel>();
        }
    }
}
