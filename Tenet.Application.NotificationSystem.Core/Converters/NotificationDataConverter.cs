﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Text.Json;
using System.Threading.Tasks;
using Tenet.Application.NotificationSystem.Core.Models;

namespace Tenet.Application.NotificationSystem.Core.Converters
{
    public class NotificationDataConverter : JsonConverter<NotificationData>
    {
        public override NotificationData Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            using (JsonDocument doc = JsonDocument.ParseValue(ref reader))
            {

                if (doc.RootElement.TryGetProperty("to", out _))
                {
                    return JsonSerializer.Deserialize<EmailNotificationData>(doc.RootElement.GetRawText(), options);
                }
                else if (doc.RootElement.TryGetProperty("phoneNumber", out _))
                {
                    return JsonSerializer.Deserialize<SmsNotificationData>(doc.RootElement.GetRawText(), options);
                }
                else if (doc.RootElement.TryGetProperty("userId", out _))
                {
                    return JsonSerializer.Deserialize<PushNotificationData>(doc.RootElement.GetRawText(), options);
                }
                throw new JsonException("Unknown notification data type");
                //if (doc.RootElement.TryGetProperty("Type", out JsonElement typeElement))
                //{
                //    string type = typeElement.GetString();
                //    switch (type)
                //    {
                //        case "email":
                //            return JsonSerializer.Deserialize<EmailNotificationData>(doc.RootElement.GetRawText(), options);
                //        case "sms":
                //            return JsonSerializer.Deserialize<SmsNotificationData>(doc.RootElement.GetRawText(), options);
                //        case "push":
                //            return JsonSerializer.Deserialize<PushNotificationData>(doc.RootElement.GetRawText(), options);
                //        default:
                //            throw new JsonException($"Unknown notification data type: {type}");
                //    }
                //}
                //throw new JsonException("Missing type discriminator property");
            }
        }

        public override void Write(Utf8JsonWriter writer, NotificationData value, JsonSerializerOptions options)
        {
            JsonSerializer.Serialize(writer, (object)value, value.GetType(), options);
        }
    }
}
