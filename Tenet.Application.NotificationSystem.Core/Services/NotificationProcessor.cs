﻿using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tenet.Application.NotificationSystem.Core.Interfaces;

namespace Tenet.Application.NotificationSystem.Core.Services
{
    public class NotificationProcessor : BackgroundService
    {
        private readonly IEventQueue _eventQueue;
        private readonly ITemplateEngine _templateEngine;
        private readonly IEnumerable<INotificationChannel> _channels;
        private readonly Dictionary<string, INotificationChannel> _channelMap;

        public NotificationProcessor(IEventQueue eventQueue, ITemplateEngine templateEngine, IEnumerable<INotificationChannel> channels)
        {
            _eventQueue = eventQueue;
            _templateEngine = templateEngine;
            _channels = channels;
            _channelMap = channels.ToDictionary(c => c.Name, c => c);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {

            while (!stoppingToken.IsCancellationRequested)
            {
                var notificationEvent = _eventQueue.Dequeue();
                if (notificationEvent != null && notificationEvent.Channels.Count() > 0)
                {
                    foreach (var channel in notificationEvent.Channels)
                    {
                        if (_channelMap.TryGetValue(channel.Type, out var notificationChannel))
                        {
                            var content = _templateEngine.GenerateContent(notificationEvent.Type, channel.Type, channel.Data);
                             
                            notificationChannel.SendNotification(content);
                        }
                    }
                }

                await Task.Delay(1000, stoppingToken);
            }

        }

        public override async Task StopAsync(CancellationToken cancellationToken)
        {
            await base.StopAsync(cancellationToken);
        }
    }

}
