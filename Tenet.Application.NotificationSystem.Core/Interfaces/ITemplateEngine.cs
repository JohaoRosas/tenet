﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tenet.Application.NotificationSystem.Core.Entities;
using Tenet.Application.NotificationSystem.Core.Models;

namespace Tenet.Application.NotificationSystem.Core.Interfaces
{
    public interface ITemplateEngine
    {
        INotificationContent GenerateContent(string eventType, string channelName, NotificationData data);
    }

    public class TemplateEngine : ITemplateEngine
    {
        public INotificationContent GenerateContent(string eventType, string channelName, NotificationData data)
        {
            switch (data)
            {
                case EmailNotificationData emailData:
                    return new EmailNotificationContent
                    {
                        Subject = emailData.Subject,
                        Body = emailData.Body,
                        To = emailData.To
                    };

                case SmsNotificationData smsData:
                    return new SmsNotificationContent
                    {
                        Message = smsData.Message,
                        PhoneNumber = smsData.PhoneNumber
                    };

                case PushNotificationData pushData:
                    return new PushNotificationContent
                    {
                        Title = pushData.Title,
                        Message = pushData.Message,
                        UserId = pushData.UserId
                    };

                default:
                    return null; // Maneja aquí el caso cuando el tipo de NotificationData no es reconocido
            }
        }
    }
}
