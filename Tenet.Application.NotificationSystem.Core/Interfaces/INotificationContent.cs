﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tenet.Application.NotificationSystem.Core.Interfaces
{
    public interface INotificationContent
    {
        string GetContentAsString(); 
    }

    public class EmailNotificationContent : INotificationContent
    {
        public string Subject { get; set; }
        public string Body { get; set; }
        public string To { get; set; }

        public string GetContentAsString()
        {
            return $"Email Subject: {Subject}\nEmail Body: {Body}";
        }
    }

    public class SmsNotificationContent : INotificationContent
    {
        public string Message { get; set; }
        public string PhoneNumber { get; set; }

        public string GetContentAsString()
        {
            return $"SMS Message: {Message}\nEmail PhoneNumber: {PhoneNumber}";
        }
    }

    public class PushNotificationContent : INotificationContent
    {
        public string Title { get; set; }
        public string Message { get; set; }
        public string UserId { get; set; }

        public string GetContentAsString()
        {
            return $"Push Title: {Title}\nPush Message: {Message}";
        }
    }
}
