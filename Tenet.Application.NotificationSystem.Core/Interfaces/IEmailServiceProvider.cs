﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tenet.Application.NotificationSystem.Core.Entities;

namespace Tenet.Application.NotificationSystem.Core.Interfaces
{
    public interface IEmailServiceProvider
    {
        void Send(EmailMessage message);
    }
}
