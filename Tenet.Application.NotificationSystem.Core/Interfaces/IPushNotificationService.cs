﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tenet.Application.NotificationSystem.Core.Interfaces
{
    public interface IPushNotificationService
    {
        void Send(string deviceToken, string message);
    }
}
