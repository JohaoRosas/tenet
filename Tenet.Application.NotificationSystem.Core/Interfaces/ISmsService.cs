﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tenet.Application.NotificationSystem.Core.Interfaces
{
    public interface ISmsService
    {
        void Send(string phoneNumber, string message);
    }
}
