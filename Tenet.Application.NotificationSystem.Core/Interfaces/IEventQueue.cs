﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tenet.Application.NotificationSystem.Core.Entities;

namespace Tenet.Application.NotificationSystem.Core.Interfaces
{
    public interface IEventQueue
    {
        void Enqueue(Event _event);
        Event Dequeue();
    }
}
