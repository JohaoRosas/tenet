# Notification System

This project implements a multi-channel notification system using .NET Core. It allows sending notifications via email, SMS, and push notifications, managing various events that can trigger these notifications.

## Features

- **Multi-Channel Support**: Capability to send notifications through different channels such as email, SMS, and push notifications.
  
- **Event-Driven Notifications**: Notifications are triggered by specific events within the system.
  
- **Flexible Content Management**: Supports customizable content templates for each notification type and channel.

## Project Structure

The project is organized into two main components:

- **Core**: Contains interfaces and models defining the system contracts.
  
- **Infrastructure**: Concrete implementations of services such as email sending (`SmtpEmailService`), SMS sending (`SmsApiService`), and push notification services (`PushNotificationApiService`).

## Prerequisites

- .NET Core SDK 3.1 or higher installed on your system.



## Features
By default, the project uses SendGrid for sending emails. To switch to another provider , adjust the configuration in Program.cs.
Configure SMS and Push Notification Providers:

SMS (SmsApiService) and push notification (PushNotificationApiService) services are registered in Program.cs. Ensure to configure appropriate credentials and API keys for the services you're using.

Access API Documentation:

The API documentation is available at http://localhost:7185/swagger when the server is running.

Contributions
Contributions are welcome! If you'd like to improve this project, follow these steps:

Fork the repository.
Create a new branch (git checkout -b feature/awesome-feature).
Make your changes and commit (git commit -am 'Add awesome feature').
Push the branch (git push origin feature/awesome-feature).
Open a Pull Request.

Feel free to customize this README according to the specific details of your project, such as service names, configuration specifics, and installation requirements. This will help users and contributors quickly understand the purpose and basic operation of your notification system.

## Setup

1. **Clone the Repository**:

   ```bash
   git clone https://gitlab.com/JohaoRosas/tenet/
   cd tenet

2. **Build and Run**:

   ```bash
   cd Tenet.Application.NotificationSystem
   Open with Visual studio