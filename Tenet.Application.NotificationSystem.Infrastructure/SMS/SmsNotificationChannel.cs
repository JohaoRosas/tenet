﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tenet.Application.NotificationSystem.Core.Interfaces;
using Tenet.Application.NotificationSystem.Core.Models;

namespace Tenet.Application.NotificationSystem.Infrastructure.SMS
{
    public class SmsNotificationChannel : INotificationChannel
    {
        private readonly ISmsService _smsService;
        public string Name => "sms";
        public SmsNotificationChannel(ISmsService smsService)
        {
            _smsService = smsService;
        }

        public void SendNotification(INotificationContent notificationContent)
        {
            if (notificationContent is SmsNotificationContent smsContent)
            {
                string message = smsContent.Message;
                string phoneNumber = smsContent.PhoneNumber;
                _smsService.Send(phoneNumber, message);
            }
           
        }
    }

}
