﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tenet.Application.NotificationSystem.Core.Interfaces;

namespace Tenet.Application.NotificationSystem.Infrastructure.SMS
{
    public class SmsApiService : ISmsService
    {
        private readonly ISmsServiceProvider _provider;
        public SmsApiService(ISmsServiceProvider provider)
        {
            _provider = provider;
        }

        public void Send(string phoneNumber, string message)
        {
            _provider.Send(phoneNumber, message);
        }
    }

}
