﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tenet.Application.NotificationSystem.Core.Entities;
using Tenet.Application.NotificationSystem.Core.Interfaces;

namespace Tenet.Application.NotificationSystem.Infrastructure.Email
{
    public class SmtpEmailService : IEmailService
    {
        private readonly IEmailServiceProvider _provider;
        public SmtpEmailService(IEmailServiceProvider provider)
        {
            _provider = provider;
        }
        public void Send(EmailMessage message)
        {
            _provider.Send(message);
        }
    }

}
