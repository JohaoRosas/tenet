﻿
using Tenet.Application.NotificationSystem.Core.Entities;
using Tenet.Application.NotificationSystem.Core.Interfaces;
using Tenet.Application.NotificationSystem.Core.Models;

namespace Tenet.Application.NotificationSystem.Infrastructure.Email
{
    public class EmailNotificationChannel : INotificationChannel
    {
        private readonly IEmailService _emailService;
        public string Name => "email";
        public EmailNotificationChannel(IEmailService emailService)
        {
            _emailService = emailService;
        }

        public void SendNotification(INotificationContent notificationContent)
        {
            if (notificationContent is EmailNotificationContent emailContent)
            {
                 
                string subject = emailContent.Subject;
                string body = emailContent.Body;
                string to = emailContent.To;

                var emailMessage = new EmailMessage
                {
                    Subject = subject,
                    Body = body,
                    To = to
                };

                _emailService.Send(emailMessage);
            }
        }
    }

}
