﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tenet.Application.NotificationSystem.Core.Interfaces;

namespace Tenet.Application.NotificationSystem.Infrastructure.PushNotifications
{
    public class PushNotificationApiService : IPushNotificationService
    {
        private readonly IPushNotificationServiceProvider _provider;
        public PushNotificationApiService(IPushNotificationServiceProvider provider)
        {
            _provider = provider;
        }
        public void Send(string deviceToken, string message)
        {
            _provider.Send(deviceToken, message);
        }
    }

}
