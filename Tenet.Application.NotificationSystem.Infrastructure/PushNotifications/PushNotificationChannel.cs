﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tenet.Application.NotificationSystem.Core.Interfaces;
using Tenet.Application.NotificationSystem.Core.Models;

namespace Tenet.Application.NotificationSystem.Infrastructure.PushNotifications
{
    public class PushNotificationChannel : INotificationChannel
    {
        private readonly IPushNotificationService _pushNotificationService;
        public string Name => "push";
        public PushNotificationChannel(IPushNotificationService pushNotificationService)
        {
            _pushNotificationService = pushNotificationService;
        }

        public void SendNotification(INotificationContent notificationContent)
        {
            if (notificationContent is PushNotificationContent pushContent)
            { 
                string title = pushContent.Title;
                string message = pushContent.Message;
                string userId = pushContent.UserId;

                _pushNotificationService.Send(userId, message);
            }
            
        }
    }

}
