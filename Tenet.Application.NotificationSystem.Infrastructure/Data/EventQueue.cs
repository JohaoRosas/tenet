﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tenet.Application.NotificationSystem.Core.Entities;
using Tenet.Application.NotificationSystem.Core.Interfaces;

namespace Tenet.Application.NotificationSystem.Infrastructure.Data
{
    public class InMemoryEventQueue : IEventQueue
    {
        private readonly Queue<Event> _events = new Queue<Event>();

        public void Enqueue(Event _event)
        {
            lock (_events)
            {
                _events.Enqueue(_event);
            }
        }

        public Event Dequeue()
        {
            lock (_events)
            {
                return _events.Count > 0 ? _events.Dequeue() : null;
            }
        }
    }
}

