﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tenet.Application.NotificationSystem.Core.Interfaces;

namespace Tenet.Application.NotificationSystem.Infrastructure.Providers.PushNotifications
{
    public class AndroidPushNotificationServiceProvider : IPushNotificationServiceProvider
    {
        public void Send(string deviceToken, string message)
        { 
            Console.WriteLine($"Sending push notification to Android device {deviceToken}: {message}");
        }
    }
}
