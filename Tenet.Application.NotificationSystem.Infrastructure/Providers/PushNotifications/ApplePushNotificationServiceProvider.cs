﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tenet.Application.NotificationSystem.Core.Interfaces;

namespace Tenet.Application.NotificationSystem.Infrastructure.Providers.PushNotifications
{
    public class ApplePushNotificationServiceProvider : IPushNotificationServiceProvider
    {
        public void Send(string deviceToken, string message)
        { 
            Console.WriteLine($"Sending push notification to iOS device {deviceToken}: {message}");
        }
    }
}
