﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tenet.Application.NotificationSystem.Core.Entities;
using Tenet.Application.NotificationSystem.Core.Interfaces;

namespace Tenet.Application.NotificationSystem.Infrastructure.Providers.Email
{
    public class SendGridEmailServiceProvider : IEmailServiceProvider
    {
        public void Send(EmailMessage message)
        {
            Console.WriteLine($"Sending email via SendGrid to: {message.To}\nSubject: {message.Subject}\nBody: {message.Body}");
        }
    }
}
