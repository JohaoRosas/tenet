﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tenet.Application.NotificationSystem.Core.Interfaces;

namespace Tenet.Application.NotificationSystem.Infrastructure.Providers.Sms
{
    public class NexmoSmsServiceProvider : ISmsServiceProvider
    {
        public void Send(string phoneNumber, string message)
        {
            // Lógica específica para enviar SMS usando Nexmo
            Console.WriteLine($"Sending SMS to {phoneNumber} via Nexmo: {message}");
        }
    }
}
